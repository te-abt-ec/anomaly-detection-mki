import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import pywt


def sliding_window_mean(data, base, length):
    """
    Calculates the mean with sliding windows
    Choose a window length larger than the sampling frequency!
    :param data: data with index unix timestamps and column with values
    :param base: string 'D' for day, 'T' for minute and 'S' for seconds
    :param length: length of the window in number of times the base (ex base = 'D', length = 3 will have 3 day window)
    :return: dataframe with sliding mean. Column names are appended with ":SW_MEAN_{base}{length}" eg ":SW_MEAN_D3"
    """
    data.index = pd.to_datetime(data.index.get_level_values("timestamps"), unit='s')
    data_c = data.rename(columns=lambda x: x + ":SW_MEAN_" + base + str(length))
    return data_c.resample("1" + base).mean().rolling(window=length, min_periods=1).mean()


def sliding_window_mean_diff(data, base, length):
    """
    Calculates the difference with the mean with sliding windows
    Choose a window length larger than the sampling frequency!
    :param data: data with index unix timestamps and column with values
    :param base: string 'D' for day, 'T' for minute and 'S' for seconds
    :param length: length of the window in number of times the base (ex base = 'D', length = 3 will have 3 day window)
    :return: dataframe with difference with sliding mean. Column names are appended with ":SW_MEAN_DIFF_{base}{length}"
    eg ":SW_MEAN_DIFF_D3"
    """
    data.index = pd.to_datetime(data.index.get_level_values("timestamps"), unit='s')
    resampled = data.resample("1" + base).mean()
    data_c = resampled - resampled.rolling(window=length, min_periods=1).mean()
    data_c = data_c.rename(columns=lambda x: x + ":SW_MEAN_DIFF_" + base + str(length))
    return data_c


def sliding_window_sum(data, base, length):
    """
    Calculates the sum with sliding windows.
    Choose a window length larger than the sampling frequency!
    :param data: data with index unix timestamps and column with values
    :param base: string 'D' for day, 'T' for minute and 'S' for seconds
    :param length: length of the window in number of times the base (ex base = 'D', length = 3 will have 3 day window)
    :return: dataframe with sliding sum. Column names are appended with ":SW_SUM_{base}{length}" eg ":SW_SUMD3"
    """
    data.index = pd.to_datetime(data.index.get_level_values("timestamps"), unit='s')
    data_c = data.rename(columns=lambda x: x + ":SW_SUM_" + base + str(length))
    return data_c.resample("1" + base).mean().rolling(window=length, min_periods=1).sum()


def stft(data, fs, frame_size, hop):
    """
    Computes the STFT (Short-Time Fourier Transform)
    Code from:
    http://tsaith.github.io/time-frequency-analysis-with-short-time-fourier-transform.html
    :param data: Numpy array containing 1 time series
    :param fs: Sampling Rate
    :param frame_size: Frame size (window)
    :param hop: Hop size (overlap)
    :return: STFT of data
    """
    frame_samp = int(frame_size*fs)  # length of the window
    hop_samp = int(hop*fs)  # hop length: skip to next (1 for perfect sliding window)
    w = np.hanning(frame_samp)  # Hanning window
    X = np.array([np.fft.fft(w*data[i:i+frame_samp])
                  for i in range(0, len(data)-frame_samp, hop_samp)])
    return X


def istft(X, fs, T, hop):
    """
    Computes the inverse STFT (Short-Time Fourier Transform)
    Code from:
    http://tsaith.github.io/time-frequency-analysis-with-short-time-fourier-transform.html
    :param X: STFT
    :param fs: Sampling Rate
    :param T: Total time duration
    :param hop: Hop size (overlap)
    :return: data in time domain
    """
    x = np.zeros(int(T*fs))
    frame_samp = X.shape[1]
    hop_samp = int(hop*fs)

    for n,i in enumerate(range(0, len(x)-frame_samp, hop_samp)):
        x[i:i+frame_samp] += np.real(np.fft.ifft(X[n]))

    return x


def plot_spectogram(data, t_min, t_max, fs, frame_size, title='Measured signal'):
    """
    Plots a spectogram of a STFT
    Code from:
    http://tsaith.github.io/time-frequency-analysis-with-short-time-fourier-transform.html
    :param data: STFT
    :param t_min: Minimum time
    :param t_max: Maximum time
    :param fs: Sampling rate
    :param frame_size: Frame size
    :param title: Title of plot
    :return: void
    """
    f_min = 0
    f_max = int(fs * 0.3)

    fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(20, 8))

    Fz = int(frame_size * fs * 0.3)
    axes.imshow(np.absolute(data[:, :Fz].T), origin='lower',
                aspect='auto', interpolation='nearest', extent=[t_min, t_max, f_min, f_max])
    plt.title(title)
    plt.xlabel('Time (seconds)')
    plt.ylabel('Frequency')
    plt.show()


def wavelet_transform(data):
    dwtdf_values = pd.DataFrame(data.values)
    dwtdf_db3_l1 = dwtdf_values.apply(lambda x: pywt.dwt(x, 'db3'))
    dwtdf_db3_l1.index = data.columns
    dwtdf_db3_l1 = pd.DataFrame(dwtdf_db3_l1.tolist(), columns=['cA1', 'cD1'])
    dwtdf_db3_l1_cA1 = pd.DataFrame.from_records(dwtdf_db3_l1['cA1'], index=data.columns).transpose()
    dwtdf_db3_l1_cD1 = pd.DataFrame.from_records(dwtdf_db3_l1['cD1'], index=data.columns).transpose()
    return dwtdf_db3_l1, dwtdf_db3_l1_cA1, dwtdf_db3_l1_cD1


def plot_wavelet(data, col_name, l1_cA1, l1_cD1):
    # reconstruction
    df_db3_l1_A1 = pd.DataFrame(pywt.idwt(l1_cA1, None, 'db3', axis=0))
    df_db3_l1_D1 = pd.DataFrame(pywt.idwt(None, l1_cD1, 'db3', axis=0))

    df_db3_l1_A1.index += 1  # add 1 to each index so they are equal with the df indices
    df_db3_l1_D1.index += 1
    df_db3_l1_A1.columns = data.columns
    df_db3_l1_D1.columns = data.columns

    fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(15, 10))
    data[col_name].plot(ax=axes[0, 0], title='Orginal', legend=None, marker='.')
    (df_db3_l1_A1[col_name] + df_db3_l1_D1[col_name]).plot(ax=axes[0, 1],
                                                           title='Approximation+Detail',
                                                           legend=None,
                                                           marker='.')
    df_db3_l1_A1[col_name].plot(ax=axes[1, 0], title='Reconstructed Approximation', legend=None,
                                                  marker='.')
    df_db3_l1_D1[col_name].plot(ax=axes[1, 1], title='Reconstructed Detail', legend=None, marker='.')

