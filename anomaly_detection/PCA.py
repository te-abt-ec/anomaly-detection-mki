import matplotlib.pyplot as plt
import numpy as np

from sklearn.decomposition import PCA


def create_pca(n_components=2):
    pca = PCA(n_components=n_components)
    return pca


def transform_data(data, pca):
    return pca.fit(data).transform(data)


def plot_pca(data_pca, title='PCA of dataset'):
    plt.figure()
    plt.scatter(data_pca[:, 0], data_pca[:, 1], color='navy', alpha=.8)
    plt.title(title)
    plt.show()


def plot_anomalies(data_pca, anomaly_idx, title='PCA plot with anomalies'):
    plt.figure()
    # Plot the training data in blue dots
    plt.scatter(data_pca[:, 0], data_pca[:, 1], color='navy', alpha=.8)
    anomalies = data_pca[anomaly_idx]
    plt.scatter(anomalies[:, 0], anomalies[:, 1], marker='x', color='red')
    plt.title(title)
    plt.show()