import copy

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from pandas.tseries.offsets import *


class AnomalySegmenter:
    """
    Class that segments resulting anomalies from a density
    """

    def __init__(self, max_distance=pd.Timedelta(12, unit='h'),
                 max_segment_expansion=pd.Timedelta(12, unit='h'),
                 boundary=pd.Timedelta(12, unit='h')):
        """
        Inits the object
        :param max_segment_expansion: max size after which no expansion of segments will occur
        :param max_distance: max distance from a timestamp to a cluster to be added to it
        :param boundary: zone around each segments that gets discarded after each incremental update
        """
        self.max_segment_expansion = max_segment_expansion
        self.max_distance = max_distance
        self.boundary = boundary
        self.segments = []

    def segments_generator(self, df, anomaly_sizes, method="mean"):
        """
        Creates a generator to incrementally update the segments.
        Some timestamps are discarded after each step!
        Usage: Option 1:    var = segments_generator()
                            result1 = var.next()
                            result2 = var.next()
               Option2:     all_results = list(segments_generator)
        :param df: df containing a datetimeindex and column 'log_prob'.
                Needs to be sorted, see GMM.add_sorted_anomaly_probs
        :param anomaly_sizes: sizes of the end index in each step eg [50, 100, 150]
        :param method: see Segment.get_distance_from_ts
        """
        self.segments = []
        start_idx = 0
        for end_idx in anomaly_sizes:
            anom_slice = df.iloc[start_idx:end_idx]
            anom_slice = self.expand_segments(anom_slice)
            anom_slice = self.discard_timestamps(anom_slice)
            self.create_segments(anom_slice, method)
            start_idx = end_idx
            # copy is necessary, df will be updated in next increment
            result = copy.deepcopy(self.segments)
            yield result

    def create_segments(self, df, method="mean"):
        """
        Creates segments in a single step.
        :param df: df containing a datetimeindex and column 'log_prob'.
                Needs to be sorted, see GMM.add_sorted_anomaly_probs
        :param method: see Segment.get_distance_from_ts
        :return: list of Segment
        """
        new_segments = []
        for anom in df.index:
            min_distance = pd.Timedelta(1, unit='Y')
            min_segment = -1
            # calculate distance to every segment
            # save min distance and the segment it belongs to
            for i in range(0, len(new_segments)):
                segment = new_segments[i]
                distance = abs(segment.get_distance_from_ts(anom, method))
                if distance <= self.max_distance and distance < min_distance:
                    min_distance = distance
                    min_segment = i

            # add anomaly to segment or create new segment
            if min_segment != -1:
                segment = new_segments[min_segment]
                segment.add_entry(df.loc[[anom]])
            else:
                new_segments.append(Segment(pd.DataFrame(df.loc[[anom]])))
        self.segments.extend(new_segments)
        return self.segments

    def discard_timestamps(self, df):
        """
        Discards all timestamps within self.boundary range of existing segments
        unless segment is smaller than max_segment_expansion
        :param df:df containing a datetimeindex and column 'log_prob'.
                Needs to be sorted, see GMM.add_sorted_anomaly_probs
        :return: df without discarded entries
        """
        invalid_anoms = []
        for anom in df.index:
            for segment in self.segments:
                if segment.get_length() >= self.max_segment_expansion:
                    # order matters! both are now positive and have to be checked between 0 and boundary
                    start_diff = segment.get_min_ts() - anom
                    end_diff = anom - segment.get_max_ts()
                    if (pd.Timedelta(0, unit='h') <= start_diff <= self.boundary) or (
                                    pd.Timedelta(0, unit='h') <= end_diff <= self.boundary):
                        invalid_anoms.append(anom)
                        break

        return df[~df.index.isin(invalid_anoms)]

    def expand_segments(self, df, method='mean'):
        """
        Updates existing segments by adding the timestamps in df that are within the boundaries of a segment to it.
        Also expands segments smaller than max_segment_expansion
        :param method: see Segment.get_distance_from_ts
        :param df: df containing a datetimeindex and column 'log_prob'.
                Needs to be sorted, see GMM.add_sorted_anomaly_probs
        :return: df with the remaining, unsegmented timestamps, NOT THE UPDATED SEGMENTS!
        """
        non_segmented = []
        for anom in df.index:
            segmented = False
            for i in range(0, len(self.segments)):
                segment = self.segments[i]
                start = segment.get_min_ts()
                end = segment.get_max_ts()
                if start <= anom <= end:
                    segment.add_entry(df.loc[[anom]])
                    segmented = True
                    break
                elif segment.get_length() < self.max_segment_expansion:
                    distance = abs(segment.get_distance_from_ts(anom, method))
                    if distance <= self.max_distance:
                        segment.add_entry(df.loc[[anom]])
                        segmented = True
                        break
            if not segmented:
                non_segmented.append(anom)
        df = df.loc[non_segmented]
        return df


class Segment:
    def __init__(self, entries):
        """
        Creates a segment object
        :param entries: pandas dataframe with a datetimeindex and a column named "log_prob"
                        see GMM.add_sorted_anomaly_probs
        """
        self.entries = entries

    def get_min_ts(self):
        """
        :return: the minimum timestamp in the index
        """
        return self.entries.index.min()

    def get_max_ts(self):
        """
        :return: the maximum timestamp in the index
        """
        return self.entries.index.max()

    def get_mean_ts(self):
        """
        :return: the mean timestamp in the index
        """
        # there is no mean method for DatetimeIndex
        start = self.get_min_ts()
        tdeltas = [ts - start for ts in self.entries.index]
        total = pd.Timedelta(0, unit='h')
        for tdelta in tdeltas:
            total += tdelta
        mean = start + total / len(tdeltas)
        return mean

    def get_length(self):
        """
        :return: length of the segment: max - min timestamp
        """
        return self.get_max_ts() - self.get_min_ts()

    def get_nb_anomalies(self):
        """
        :return: number of anomalies in segment
        """
        return self.entries.shape[0]

    def get_score(self):
        """
        :return: mean log probability of segment
        """
        return self.entries['log_prob'].sum()

    def add_entry(self, entry):
        """
        Adds an entry to the Segment
        :param entry: row of pd.DataFrame
        """
        self.entries = self.entries.append(entry)

    def get_distance_from_ts(self, timestamp, method):
        """
        Calculates distance from timestamp to segment with a given method as timestamp - segment
        :param timestamp: Timestamp
        :param method: "edge" (min distance to max or min timestamp in index),
                       "start" (distance to earliest timestamp in index),
                       "end" (distance to latest timestamp in index),
                       "mean" (distance to mean timestamp in index)
        :return: distance (Timedelta)
        """
        if method == "start":
            return timestamp - self.get_min_ts()
        elif method == "end":
            return timestamp - self.get_max_ts()
        elif method == "edge":
            return min(timestamp - self.get_min_ts(), timestamp - self.get_max_ts())
        elif method == "mean":
            mean = self.get_mean_ts()
            return timestamp - mean
        else:
            raise ValueError("Method should be 'start', 'end', 'edge' or 'mean'")

    def format_ts(self):
        """
        Formats the index into a printable format (sorted)
        :return: Index sorted and formatted
        """
        return self.entries.sort_index().index.format(formatter=lambda x: x.strftime('%Y-%m-%d %H:%M.%S'))


def compare_segmented_with_labeled(segments_B1, segments_B2, labeled, method='start'):
    labeled_w_segments = pd.DataFrame(index=labeled.index.copy(),
                                      columns=['nb_detected_B1', 'nb_segments_B1',
                                               'nb_detected_B2', 'nb_segments_B2',
                                               'score', 'installation'])
    labeled_w_segments = labeled_w_segments.fillna(0)
    labeled_w_segments['installation'] = labeled['VALUE']
    max_distance = pd.Timedelta(12, unit='h')
    for label in labeled_w_segments.index:
        for segment in segments_B1:
            distance = segment.get_distance_from_ts(label, method)
            if pd.Timedelta(0, unit='h') <= distance <= max_distance:
                labeled_w_segments.loc[label, 'nb_detected_B1'] += segment.get_nb_anomalies()
                labeled_w_segments.loc[label, 'nb_segments_B1'] += 1
                labeled_w_segments.loc[label, 'score'] += segment.get_score()
        for segment in segments_B2:
            distance = segment.get_distance_from_ts(label, method)
            if pd.Timedelta(0, unit='h') <= distance <= max_distance:
                labeled_w_segments.loc[label, 'nb_detected_B2'] += segment.get_nb_anomalies()
                labeled_w_segments.loc[label, 'nb_segments_B2'] += 1
                labeled_w_segments.loc[label, 'score'] += segment.get_score()

    labeled_w_segments['score'] = labeled_w_segments['score'] / (
        labeled_w_segments['nb_segments_B1'] + labeled_w_segments['nb_segments_B2'])
    labeled_w_segments.fillna(0, inplace=True)
    return labeled_w_segments


def compare_detected_w_labeled(detected, labeled):
    """
    Compares the detected anomalies with labeled ones with 12 hour period
    :param detected: Dataframe with index the timestamps of detected anomalies
    :param labeled: Dataframe with index the timestamps of labeled anomalies
    :return: 2 dataframes, one with the detected timestamps and whether or not they correspond to a label.
    Second dataframe with labeled timestamps and how many detected for each label
    """
    detected_w_label = pd.DataFrame(index=detected.index.copy(), columns=['has_label'])
    detected_w_label = detected_w_label.fillna(False)
    labeled_w_detected = pd.DataFrame(index=labeled.index.copy(), columns=['nb_detected'])
    labeled_w_detected = labeled_w_detected.fillna(0)
    for end in labeled_w_detected.index:
        start = end - DateOffset(hours=12)
        for detected_ts in detected_w_label.index:
            if start <= detected_ts <= end:
                detected_w_label.loc[detected_ts, 'has_label'] = True
                labeled_w_detected.loc[end, 'nb_detected'] += 1
    return detected_w_label, labeled_w_detected


def plot_segments_series(data, series_name, segments, labels=None, title='Segment centers plotted on timeseries'):
    fig, (ax1, ax2) = plt.subplots(nrows=2, ncols=1, figsize=(12, 8), sharex=True)
    regex = '^MKI.*:%s$' % series_name
    timeseries = data.filter(regex=regex)
    timeseries.plot(ax=ax1, colormap='winter', kind='line', linestyle='solid')

    ax2_title = "Detected segments"
    if labels is not None:
        for label in labels.index:
            ax2.axvline(label, ymin=0, ymax=0.3, color='green')
        ax2_title = "Detected segments (red) and labels (green)"

    segment_centers = [segment.get_mean_ts() for segment in segments]
    X = -1 * np.array([segment.get_score() for segment in segments])
    X_std = (X - X.min()) / (X.max() - X.min())
    segment_scores = X_std * (1 - 0.31) + 0.31

    for center, score in zip(segment_centers, segment_scores):
        plt.axvline(center, ymin=0.3, ymax=score, color='red')
        # plt.text(center, 1, score, rotation='vertical')
    fig.suptitle(title, fontsize=14, fontweight='bold')
    ax1.set_title("Timeseries")
    ax2.set_title(ax2_title)
