import matplotlib.pyplot as plt

import data_exploration as de
from database import mongo_client

client = mongo_client.CERNMongoClient(logging=False)
all_collections = client.get_all_collections()
print("Found: " + str(len(all_collections)) + " collections:")
print(all_collections)

df = client.get_measurements_pattern("^.*PRESSURE$")
df = client.compress_measurements(df)
plot = de.plot_B1_B2(df, "PRESSURE", "mbar")
plt.show()

df = client.get_measurements_pattern("^.*TEMP_TUBE_UP$")
df = client.filter_extremes(df, 150)
df = client.compress_measurements(df)
plot = de.plot_B1_B2(df, "TEMP_TUBE_UP", "C")
plt.show()

data = client.get_measurements_pattern("^.*E_KICK")
plot = de.plot_IPOC(data, "E_KICK", "GeV")
plt.show()



