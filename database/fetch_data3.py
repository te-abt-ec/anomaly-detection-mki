#!/usr/bin/python3.4
#previously on dfs: /user/bdisoft/operational/bin/Python/PRO/bin/python
#encoding: utf-8
"""
Created by Pieter Van Trappen, CERN
Extract data from CERN's logging database and write to a JSON file
Also writing into a Redis or MongoDb database is possible.
-------------------------------
v1.0 : used succesfully in 2016
v1.1 : 22/02/2017: removed PRESSURE_INT variable queries
-------------------------------
"""
import sys
import pytimber, time
import json
import numpy as np
import tarfile
import redis
import ast
import os
import re
import pymongo
from datetime import datetime
from datetime import timedelta
from pymongo import MongoClient

class mongoDB:
    def __init__(self,db='cern_ldb'):
        self.conn = MongoClient()
        self.db = self.conn[db]
        self.index_name = 'start-time'
        
    def write_doc(self, var, data, startt, samplesize):
        """
        This function writes the [[ts1,][v1,]] data and other arguments to a mongoDB
        so-called document in the given collection (i.e. LDB variable). A dictionary
        is created following the below structure.
        """
        one = {"start-time":startt, "length":samplesize, "timestamps":data[0],
               "values":data[1]}
        coll = self.db[var]
        
        try:
            coll.insert_one(one)
            return 0
        except pymongo.errors.DocumentTooLarge as e:
            print("16MB exceeded!")
            print(e)
            return 1

    def write_bulk(self, var, data, samplesize):
        """
        This function writes the [[ts1,][[a1,],]]] data and other arguments to mongoDB
        so-called documents in the given collection (i.e. LDB variable) while using a
        single ts,array combination per doucment. All n combinations are written als bulk
        to the n documents. A dictionary is created following the below structure.
        Timestamps and start-time keys are the same but different representations.
        Existing documents are omitted when writing to mongoDB.
        """
        bulk = []
        for i,v in enumerate(data[0]):
            # iterating over the timestamps
            sts = datetime.fromtimestamp(v).strftime('%Y-%m-%d %H:%M:%S.%f')
            one = {"start-time":sts, "length":samplesize, "timestamps":v,
                   "values":data[1][i]}
            if not self.exist_doc(var,sts,samplesize):
                bulk.append(one);
            else:
                print("Existing document for %s with ts %s, omitting" % (var, sts))
        if len(bulk)==0:
            return 2
        coll = self.db[var]
        try:
            coll.insert_many(bulk)
            return 0
        except pymongo.errors.DocumentTooLarge as e:
            print("16MB exceeded!")
            print(e)
            return 1
        
    def exist_doc(self, var, startt, samplesize):
        colls = self.db.collection_names()
        if (var in colls):
            coll = self.db[var]
        else:
            return False
        if coll.count()=='0':
            return False
        one = {"start-time":startt, "length":samplesize}
        return True if (coll.find(one).count()>0) else False

class redisDB:
    def __init__(self, host='localhost'):
        self.conn = redis.Redis(host)
        if not self.conn.ping():
            print("no redis server found")

    def write_hash(self, rd_key, rd_hash):
        """
        This function writes the hash, being a dictionary or [[ts,][val,]] list
        of lists as extracted from e.g. CERN LDB to the redis db. Each
        python key-value pair will become a hash' field-value
        pair.
        """
        if isinstance(rd_hash,dict): # retests, does this work?
            for k,v in rd_hash.items():
                self.conn.hset(rd_key, k, v)
        else:
            for i,v in enumerate(rd_hash[0]):
                self.conn.hset(rd_key, v, rd_hash[1][i])

    def fetch_convert(self, rd_key, rd_field):
        """
        fetches and converts string to original [[][]] value
        """
        l = self.conn.hget(rd_key, rd_field)
        l = l.decode("utf-8")
        return ast.literal_eval(l)

    def create_key(self, vars, dt, version):
        """
        creates a key and check if it exists
        dt should be a list/tuple of datetime objects
        """
        st = datetime.strftime(dt[0],'%Y%m%d')
        et = datetime.strftime(dt[1], '%Y%m%d')
        rd_key = vars+"_"+version+"_"+st+"_"+et
        if self.conn.exists(rd_key):
            print("redis key %s exists, do you want to possibly overwrite hash-fields (y,n)?" % rd_key)
            uchoice = input()
            if uchoice=='n':
                sys.exit()
        return rd_key

class loggingDB:
    all_vars = []
    def __init__(self):
        # for now ldb only, stronger filtering hence less data hence less time
        self.db = pytimber.LoggingDB('LHC_MD_ABP_ANALYSIS','BEAM PHYSICS','ldb')
    
    def build_vars(self, type='o'):
        """
        builds a list of lists of LDB variables to querry.
        Make sure that the the [[],] structure is kept because
        of iterations later on.
        type: o for others, w for waveforms
        """
        mkiB1 = "MKI.UA23"
        mkiB2 = "MKI.UA87"
        magnets = ("A","B","C","D")
        vars_plc = (".STATE:MODE", ".STATE:CONTROL", ".STATE:SOFTSTARTSTATE", ".STATE:STATUS")
        vars_ipoc = (":E_KICK",":T_DELAY",":T_FALLTIME",":T_LENGTH",":T_RISETIME",":T_START_TH",":I_STRENGTH") #":TIMESTAMP" removed because long?
        varst = self.db.tree.LHC.Kickers.Injection.Beam1.Magnets.get_vars() # returns a list with TEMP etc.
        vars_magnets1 = [v for v in varst if not 'PRESSURE_INT' in v]
        varst = self.db.tree.LHC.Kickers.Injection.Beam2.Magnets.get_vars()
        vars_magnets2 = [v for v in varst if not 'PRESSURE_INT' in v]
        vars_beami = self.db.search("LHC.BCTFR.A6R4.B%:BEAM_INTENSITY") # only one % per list it seems
        vars_bunchl = self.db.search("LHC.BQM.B%:BUNCH_LENGTH_MEAN")
        vars_kits = (".F3.CONTROLLER:KICK_COUNT_TOPLAY",".F3.CONTROLLER:KICK_DELAY_TOPLAY",".F3.CONTROLLER:KICK_ENABLE_TOPLAY",".F3.CONTROLLER:KICK_LENGTH_TOPLAY",".F3.CONTROLLER:KICK_STRENGTH_TOPLAY",".F3.CONTROLLER:KICK_TIME_TOPLAY")
        plc_mkiB1 = [mkiB1+s for s in vars_plc]
        plc_mkiB2 = [mkiB2+s for s in vars_plc]
        ipoc_mkiB1 = [mkiB1+".IPOC."+m+"B1"+v for m in magnets for v in vars_ipoc]
        ipoc_mkiB2 = [mkiB2+".IPOC."+m+"B2"+v for m in magnets for v in vars_ipoc]
        kits_mkiB1 = [mkiB1+v for v in vars_kits]
        kits_mkiB2 = [mkiB2+v for v in vars_kits]
        # vars for waveforms
        vars_eqpm = ("TMR-",)
        wave_mkiB1 = [mkiB1+".IPOC."+e+m+"B1" for e in vars_eqpm for m in magnets]      
        # combine all variables in a list
        if type=='o':
            self.all_vars = [plc_mkiB1, plc_mkiB2, vars_magnets1, vars_magnets2, ipoc_mkiB1, ipoc_mkiB2, vars_beami, vars_bunchl, kits_mkiB1, kits_mkiB2]
        elif type=='w':
            self.all_vars = [wave_mkiB1]
        return self.all_vars

    def fetch_data(self, variables, startt=time.time()-36000, endt=time.time()):
        dat = self.db.get(variables,startt,endt)
 #       print(dat)
        return dat

    def write_json(self, dat, fn):
        if dat:
            with open(fn, 'w') as of:
                json.dump(dat, of, indent=None, separators=(',',':\n'))
        else:
            print("Writing out an empty dictionary aborted")

    def nparray_to_list(self, dat, prevDic=None):
        """
        This function converts a dictionary of loggingDB key,tuple of NumPy arrays
        to a dictionary of loggingDB values with key,list - understood by JSON.
        If prevDic is provided, the dat dictionary lists [0] and [1] will 
        be appended to the prevDic ones, provided the same key exists. That
        appended dic will than be returned.
        """
        datl = {}
        for key,value in dat.items():
            if (prevDic is not None) and prevDic:  # returns true when not empty
                #list.extend() not working ?
                datl[key] = ( prevDic[key][0]+value[0].tolist(), prevDic[key][1]+value[1].tolist() )
            else:
                datl[key] = ( value[0].tolist(), value[1].tolist() )
        return datl

    def tarball(self, fn):
        tar = tarfile.open(fn+"tar.gz", "w:gz")
        tar.add(fn)
        tar.close()

    def filename(self, vars, dt, version):
        """
        dt should be a list/tuple of datetime objects
        """
        st = datetime.strftime(dt[0],'%Y%m%d')
        et = datetime.strftime(dt[1], '%Y%m%d')
        fn = vars+"_"+version+"_"+st+"_"+et+".json"
        if os.path.isfile(fn):
            print("filename %s exists, do you want to overwrite (y,n)?" % fn)
            uchoice = input()
            if uchoice=='n':
                sys.exit()
        return fn

def userdates(defaults, mode='j', mdays='5'):
    """
    defaults should be a list of default start-
    and end-dates as string.
    Returns a list with two-or-more datetime objects.
    In case of mongodb, the list will contain all
    timeintervals
    """
    ret = []
    for i,v in enumerate(defaults):
        d = "start" if i==0 else "end"
        s = input("Please provide a %s-date (%s): " % (d,v))
        ret.append(datetime.strptime(v if s=="" else s, '%Y-%m-%d %H:%M:%S.%f'))
    if mode=='m':
        print("mongoDB-mode, end-date is being ignored")
        ret.pop()
        s = int(input("Please provide the amount of %d-day timespans to query: " % mdays), 10)
        for i in range(1,s+1):
            ret.append(ret[i-1] + timedelta(days=mdays))
        #print(ret)
        print("fyi, end-time is %s" % datetime.strftime(ret[-1], '%Y-%m-%d %H:%M:%S.%f'))
    return ret

if __name__ == '__main__':
    print("connecting to the logging-DB LDB...")
    log = loggingDB()

    wo = input("Choose between json, redis or mongoDB writeout (j,r,m): ")
    if not (wo=='j' or wo=='r' or wo=='m'):
        print("invalid choice, exiting")
        sys.exit()
    if wo=='r':
        conn = redisDB()
    elif wo=='m':
        conn = mongoDB()

    # set the start- and end-times
    startt = "2016-07-01 00:00:00.000"
    endt = "2016-07-05 00:00:00.000"
    now = time.time()
    now_min_1d = now-(60*60*24)
     # max days for mongodb
    mddays = 5

    #TODO: if no variables found, don't overwrite existing file again
    #TODO: make sure tee works or enable logging here
    #TODO: print time when starting and when exiting
    MAX_QUERY_WAVE = 50 # 20 seems to equal the ~250 MB java heap limit; 10 because accumulated heap violation
    MAX_QUERY_TIME = 1800 # for unix time comparison, in seconds
    uchoice = input("Menu: w for waveform data, o for others: ")
    if uchoice=="w":
        dt = userdates((startt,endt)) # don't pass mode, we dont want x-days intervals for mongodb
        # get vars and filename/key
        vars_all = log.build_vars('w')
        if wo=='j':
            fn = log.filename("tmr-test", dt, "v0_2")
        elif wo=='r':
            rk = conn.create_key("tmr-test", dt, "v0_2")
        # loop over all vars
        uchoice = ''
        for wl in vars_all:
            for i,v in enumerate(wl):
                vars_wave = ("/Waveform#acquisitionStamp","/Waveform#waveformData")
                v1 = v+vars_wave[0]
                v2 = v+vars_wave[1]
                print("Fetching timestamps from %s" % v1)
                d = log.fetch_data(v1,dt[0],dt[1])
                wts = d[list(d)[0]][0]
                wts_len = len(wts)
                if uchoice == '':
                    # test for empty so this is asked only once (at the beginning)
                    print("%d timestamps found, do you want to fetch them all (y,n,m)?" % wts_len)
                    uchoice = input()
                    if not (uchoice=='y' or uchoice=='n' or uchoice=='m'):
                        print("invalid choice, exiting")
                        sys.exit()
                if uchoice=="n":
                    # other uchoices handled lower
                    print("printing first 30 timestamps")
                    for each in wts[:29]:
                        print(pytimber.dumpdate(each))
                    sys.exit()
                data_wave = {} # dic will contain only one key, but values need appending
                print("splitting up data")
                wts_p = []
                a = 0
                # runs till wts_len, wts_len-1 is last array pointer
                for j in range(1,wts_len):
                    if wts[j]-wts[j-1]>MAX_QUERY_TIME or j-a==MAX_QUERY_WAVE:
                        wts_p.append((a,j-1))
                        a = j
                    elif j==wts_len-1:
                        wts_p.append((a,j))
                if uchoice=='m':
                    print(wts_p)
                    """print("printing out debug info:")
                    for each in wts[120:140]:
                        print("fp %f is %s" % (each,pytimber.dumpdate(each)))"""
                    sys.exit()
                for pntr in wts_p:
                    print("Fetching between time-stamp %d and %d" % (pntr[0], pntr[1]))
                    d = log.fetch_data(v2, wts[pntr[0]], wts[pntr[1]])
                    # convert and append data dics
                    #print(d)
                    data_wave = log.nparray_to_list(d, data_wave);
                # write out
                print("Writing dictionary to file/db...")
                if wo=='j':
                    log.write_json(data_wave,fn)
                elif wo=='r':
                    # one LDB key-value pair can surpass 512MB if ~1 day data
                    # conn.write_hash(rk, data_wave)
                    conn.write_hash(v2, data_wave[v2])
                elif wo=='m':
                    # replace needed because mongodump/restore can't handle '/'
                    ret = conn.write_bulk(v2.replace('/','.'), data_wave[v2], 'single')
    elif uchoice=="o":
        dt = userdates((startt,endt),wo,mddays)
        if wo=='j':
            fn = log.filename("varset", dt, "v0_2")
        elif wo=='r':
            rk = conn.create_key("varset", dt, "v0_2")
        vars_all = log.build_vars('o')        
        data_all = {}
        uchoice = input("%d variables will be queried, please specify a regex if wanted (.*): \n" % sum(len(x) for x in vars_all))
        if not uchoice=='':
            uchoice = re.compile(uchoice)
            vars_all_f = []
            # iterate over the 2-dim list; dont merge because 2nd dim lists are sent to pytimber
            for each in vars_all:
                l = [k for k in each if re.match(uchoice, k)]
                if len(l)>0:
                    vars_all_f.append(l)
            fsum = sum(len(x) for x in vars_all_f)
            if fsum==0:
                print("Filter returned 0 variables, exiting")
                sys.exit()
            else:
                print("Filter returned %d variables" % fsum)
                vars_all = vars_all_f
        for i,v in enumerate(vars_all):
            # store original vars because list might be reduced; multiple time-stamps (j)
            v_mem = v
            for j in range(0,len(dt)-1):
                sts = datetime.strftime(dt[j],'%Y-%m-%d %H:%M:%S.%f')
                if (wo=='m'):
                    l = []
                    # reduce list of variables to query if already in mongodb
                    for each in v:
                        if (conn.exist_doc(each, sts, str(mddays)+'d')):
                           print("Existing document found for collection %s and ts %s, skipping.." % (each, sts))
                        else:
                            l.append(each)
                    v = l
                if (len(v)>0):
                    print("fetching vars-%d between %s and %s ..." % (i,dt[j],dt[j+1]))
                    print(v)
                    d = log.fetch_data(v,dt[j],dt[j+1])
                    print("done, converting array.")
                    d = log.nparray_to_list(d)
                    print("Writing dictionary to file/db...")
                    if wo=='m':
                        # v is a list of vars, d hence a dict {var, [[][]]}
                        for key,value in d.items():
                            ret = conn.write_doc(key, value, sts, str(mddays)+'d')
                            if ret==1:
                                #TODO: dictionary too big, reducing query size
                                sys.exit()
                    else:
                        for key,value in d.items():
                            data_all[key]=value
                            print("key %s has %d values" % (key, len(value[0])))
                        # write out
                        if wo=='j':
                            log.write_json(data_all,fn)
                        else:
                            conn.write_hash(rk, data_all)
                else:
                    print("Nothing to query for vars-%d, skipped." % i)
                v = v_mem
    # outside of big if

    # TODO: (re)index collections from mongoDB
    print("Exiting")
    sys.exit()

