import gc
import os
import re
import datetime

import pandas as pd
from pandas.tseries.offsets import *

from anomaly_detection import feature_extraction as fe
from anomaly_detection import preprocessing
from database import mongo_client


def load_file(filename):
    dir = os.path.dirname(__file__)
    print "Loading {0}".format(filename)
    filename = os.path.join(dir, '..', 'CSV', filename + ".csv")
    return pd.read_csv(filename, index_col=0)


def save_file(filename, df):
    dir = os.path.dirname(__file__)
    print "Saving {0}".format(filename)
    filename = os.path.join(dir, '..', 'CSV', filename + ".csv")
    df.to_csv(filename)


client = mongo_client.CERNMongoClient()
start_date = '2016-04-16 00:00:00.000000'
start_date = pd.to_datetime(start_date, infer_datetime_format=True)

IPOC_end_date = '2016-09-14 00:00:00.000000'
IPOC_end_date = pd.to_datetime(IPOC_end_date, infer_datetime_format=True)

SW_size = 1800
magnets = "(A|B|C|D)"
beams = "(B2)"

now = datetime.datetime.now()
date = now.strftime("%Y-%m-%d")
filename = "all_features_{0}_{1}".format(beams, date)

# check IPOC indices
# all = [m.group(0) for l in client.get_all_collections() for m in [re.search("^.*IPOC.(A|B|C|D)(B1).*", l)] if m]
# ids = []
# for IPOC in all:
#     df = client.query_resample(IPOC, str(start_date), str(start_date + DateOffset(months=1)))
#     ids.append(df.index.round("S"))
#
# for i in range(0, len(ids)-1):
#     print ids[i].equals(ids[i+1]), " ", all[i], " ", all[i+1]

previous_month = pd.DataFrame()
nb_months = 5

for i in range(0, nb_months):
    start = start_date + DateOffset(months=i)
    end = start + DateOffset(months=1)
    # IPOC indices go out of sync at the end, dont go beyond this point
    if end > IPOC_end_date:
        end = IPOC_end_date
    query = "^MKI.{0}.*{1}:.*".format(magnets, beams)
    new_month = client.query_resample(query, str(start), str(end), "1S")
    print "filtering month {0}".format(i)
    new_month = preprocessing.filter_extremes(new_month)
    two_months = pd.concat([previous_month, new_month], keys=["previous", "new"])
    print "Computing SWMD month {0}".format(i)
    SWMD = fe.sliding_window_mean_diff(two_months, "s", SW_size)
    print "Computing SWS month {0}".format(i)
    SWS = fe.sliding_window_sum(two_months, "s", SW_size)
    print "querying IPOC month {0}".format(i)
    query_ipoc = "^.*IPOC.{0}{1}.*".format(magnets, beams)
    joined = client.query_resample(query_ipoc, str(start), str(end))
    joined = preprocessing.filter_IPOC(joined)
    joined.index = joined.index.round('S')
    joined["timestamps"] = joined.index
    IPOC_length = len(joined)
    print "Joining month {0}".format(i)
    two_months["timestamps"] = two_months.index.get_level_values("timestamps")
    joined = pd.merge(joined, two_months, how='left')
    print "Joining SWMD month {0}".format(i)
    SWMD["timestamps"] = SWMD.index
    joined = pd.merge(joined, SWMD, how='left')
    print "Joining SWS month {0}".format(i)
    SWS["timestamps"] = SWS.index
    joined = pd.merge(joined, SWS, how='left')
    joined.index = joined["timestamps"]
    joined.drop("timestamps", axis=1, inplace=True)
    print "original length: {0}".format(IPOC_length)
    print "new length: {0}".format(len(joined))
    save_file("all_features_month{0}".format(i), joined)
    # only save tail of current month to compute next sliding window
    tail = end - DateOffset(days=1)
    previous_month = new_month[str(tail):]


all_months = [load_file("all_features_month{0}".format(i)) for i in range(0, nb_months)]
all_features = pd.concat(all_months)
save_file(filename, all_features)

# check if all data is in the frame
query_ipoc = "^.*IPOC.{0}{1}.*".format(magnets, beams)
all_IPOC = client.query_resample(query_ipoc, str(start_date), str(IPOC_end_date))
all_IPOC = preprocessing.filter_IPOC(all_IPOC)
print len(all_IPOC)
features = load_file(filename)
print len(features)
features = features.dropna()
print len(features)
save_file(filename, features)





