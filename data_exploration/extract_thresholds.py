import pandas as pd

import data_exploration as de
from database import mongo_client

client = mongo_client.CERNMongoClient(logging=False)

# pressure = client.get_measurements_pattern("^.*PRESSURE")
# pressure_anom = de.extract_between_thresholds(pressure, 3*10**-9, 1)
# json = pressure_anom.to_json("test.json")

thresholds = {"PRESSURE:": {"min": 3*10**-9,
                            "max": 1},
              "TEMP_MAGNET_UP": {"min": 300,
                                 "max": 5000},
              "TEMP_TUBE_UP": {"min": 300,
                               "max": 5000},
              "T_DELAY": {"min": 5,
                          "max": 40},
              "T_FALLTIME": {"min": 5,
                             "max": 59},
              "T_LENGTH": {"min": 25,
                           "max": 100},
              "T_RISETIME": {"min": 10,
                             "max": 150}}

pd.DataFrame(thresholds).to_json("thresholds.json")

result = pd.DataFrame()

for type in thresholds:
    min = thresholds.get(type).get("min")
    max = thresholds.get(type).get("max")
    pattern = "^.*" + type
    meas = client.get_measurements_pattern(pattern)
    meas = de.extract_between_thresholds(meas, min, max)
    result = pd.concat([result, meas], axis=1)

print result.head()

result.to_json("anomalies.json", date_format='iso')

