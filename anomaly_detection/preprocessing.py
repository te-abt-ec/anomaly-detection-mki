import re

import pandas as pd
from sklearn.preprocessing import scale, robust_scale


def filter_extremes(df):
    """
    Filters a df  on extreme outliers in temperature and pressure
    Min and max values given by Pieter
    :param df: given by query resample
    :return: filtered dataframe
    """
    pres = re.compile('MKI\..*:PRESSURE$')
    temp_tube = re.compile('MKI\..*:TEMP_TUBE.*$')
    temp_magnet = re.compile('MKI\..*:TEMP_MAGNET.*$')
    for col in df:
        if pres.match(col):
            df = df[(df[col] <= 5*10**-9) & (df[col] >= 9*10**-12)]
        elif temp_tube.match(col):
            df = df[(df[col] <= 120) & (df[col] >= 18)]
        elif temp_magnet.match(col):
            df = df[(df[col] <= 60) & (df[col] >= 18)]
    return df


def filter_IPOC(df):
    """
    Crude IPOC filtering
    Values given by Pieter
    :param df: given by query resample
    :return: filtered dataframe
    """
    i_strength = re.compile('MKI\..*:I_STRENGTH$')
    t_delay = re.compile('MKI\..*T_DELAY$')
    for col in df:
        if i_strength.match(col):
            df = df[df[col] > 1]
        elif t_delay.match(col):
            df = df[df[col] > 10]
    return df



def scale_data(df):
    """
    Scales the data by setting mean to 0 and variance to unit
    :param df: given by query_resample
    :return: scaled df
    """
    df2 = pd.DataFrame(index=df.index, data=scale(df))
    df2.columns = df.columns
    return df2


def scale_robust(df):
    """
    Scales the data in a robust manner in case of outliers
    :param df:
    :return: scaled df
    """
    df2 = pd.DataFrame(index=df.index, data=robust_scale(df))
    df2.columns = df.columns
    return df2




