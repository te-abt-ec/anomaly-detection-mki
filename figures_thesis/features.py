# -*- coding: utf-8 -*-
import re

import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm

from anomaly_detection import PCA
from anomaly_detection import feature_extraction
from anomaly_detection import preprocessing
from database import mongo_client
import numpy as np

plt.style.use('ggplot')


def create_sw_plot():
    client = mongo_client.CERNMongoClient()
    start_date = '2016-04-16 00:00:00.000000'
    end_date = '2016-09-14 00:00:00.000000'
    data = client.query_resample_collection("MKI.A5R8.B2:TEMP_MAGNET_UP", start_date, end_date, None)
    data = preprocessing.filter_extremes(data)

    min30 = feature_extraction.sliding_window_mean(data, 'T', 30)
    day = feature_extraction.sliding_window_mean(data, 'D', 1)
    day10 = feature_extraction.sliding_window_mean(data, 'D', 10)
    mean = data.mean()

    fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(15, 10))
    data.plot(ax=axes, color='b')
    min30.plot(ax=axes, color='r')
    day.plot(ax=axes, color='g')
    day10.plot(ax=axes, color='c')
    pd.Series(mean[0], index=data.index).plot(ax=axes, color='m')
    labels = ["data", "30 min", "1 dag", "10 dagen", "gemiddelde"]
    axes.legend(labels, loc='upper left')
    axes.set_ylabel(u"TEMP_MAGNET UP in °C")
    axes.set_xlabel("Tijd")
    plt.show()


def create_IPOC_hist():
    client = mongo_client.CERNMongoClient()
    start_date = '2016-04-16 00:00:00.000000'
    end_date = '2016-09-14 00:00:00.000000'
    df = client.query_resample("MKI.UA23.IPOC.AB1:T_LENGTH", start_date, end_date)
    index = df.index.round("S")
    df["timestamps"] = index
    # http://stackoverflow.com/a/26916647
    cluster = (df["timestamps"].diff() > pd.Timedelta(minutes=30)).cumsum()
    dfs = [v for k, v in df.groupby(cluster)]

    IPOC_lenghts = []
    for clust in dfs:
        if len(clust) != 1:
            IPOC_lenghts.append(clust["timestamps"].iloc[-1] - clust["timestamps"].iloc[0])

    IPOC_lenghts = pd.Series(IPOC_lenghts)
    fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(12, 8))
    (IPOC_lenghts / pd.Timedelta(minutes=1)).hist(bins=xrange(0, 200, 10))
    plt.xlabel('IPOC lengte in min')
    plt.ylabel('Aantal groepen')
    plt.show()

# create_sw_plot()
create_IPOC_hist()