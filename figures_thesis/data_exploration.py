# -*- coding: utf-8 -*-
import re

import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm

from anomaly_detection import PCA
from anomaly_detection import preprocessing
from database import mongo_client
import numpy as np

plt.style.use('ggplot')


def plot_cont_B1_B2(measurement, unit, filter=False):
    client = mongo_client.CERNMongoClient()
    start_date = '2016-04-16 00:00:00.000000'
    end_date = '2016-09-14 00:00:00.000000'
    query_B1 = "^MKI.*B1:{}".format(measurement)
    query_B2 = "^MKI.*B2:{}".format(measurement)
    data_B1 = client.query_resample(query_B1, start_date, end_date, '30T')
    data_B2 = client.query_resample(query_B2, start_date, end_date, '30T')
    if filter:
        data_B1 = preprocessing.filter_extremes(data_B1)
        data_B2 = preprocessing.filter_extremes(data_B2)
    fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, figsize=(15, 8), sharey=True)
    data_B1.plot(ax=ax1).legend(loc='upper left')
    data_B2.plot(ax=ax2).legend(loc='upper right')

    # ax1.set_ylim(15, 70)
    ax1.set_title("{}, MKI2".format(measurement))
    ax2.set_title("{}, MKI8".format(measurement))
    ax1.set_xlabel("Tijd")
    ax2.set_xlabel("Tijd")
    ax1.set_ylabel(u"{0} in {1}".format(measurement, unit))
    ax2.set_ylabel(u"{0} in {1}".format(measurement, unit))
    plt.tight_layout()
    plt.show()


def plot_IPOC_B1_B2(measurement, unit, filter=False):
    client = mongo_client.CERNMongoClient()
    start_date = '2016-04-16 00:00:00.000000'
    end_date = '2016-09-14 00:00:00.000000'
    query_B1 = "^MKI.*IPOC.(A|B|C|D).*B1.*$"
    query_B2 = "^MKI.*IPOC.(A|B|C|D).*B2.*$"
    IPOC_B1 = client.query_resample(query_B1, start_date, end_date)
    IPOC_B2 = client.query_resample(query_B2, start_date, end_date)
    if filter:
        IPOC_B1 = preprocessing.filter_IPOC(IPOC_B1)
        IPOC_B2 = preprocessing.filter_IPOC(IPOC_B2)
    fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, figsize=(15, 8), sharey=True)
    color = iter(cm.rainbow(np.linspace(0, 1, 4)))
    str_B1 = "^MKI.*IPOC.*B1:{}".format(measurement)
    str_B2 = "^MKI.*IPOC.*B2:{}".format(measurement)
    for series in IPOC_B1:
        if re.match(str_B1, series):
            c = next(color)
            ax1.scatter(IPOC_B1.index, IPOC_B1.loc[:, series], c=c)
    color = iter(cm.rainbow(np.linspace(0, 1, 4)))
    for series in IPOC_B2:
        if re.match(str_B2, series):
            c = next(color)
            ax2.scatter(IPOC_B2.index, IPOC_B2.loc[:, series], c=c)
    fig.subplots_adjust(bottom=0.23)
    ax1.legend(loc='upper center', bbox_to_anchor=(0.5, -0.08))
    ax2.legend(loc='upper center', bbox_to_anchor=(0.5, -0.08))
    ax1.set_title("{}, MKI2".format(measurement))
    ax2.set_title("{}, MKI8".format(measurement))
    ax1.set_xlabel("Tijd")
    ax2.set_xlabel("Tijd")
    ax1.set_ylabel(u"{0} in {1}".format(measurement, unit))
    ax2.set_ylabel(u"{0} in {1}".format(measurement, unit))

    # ax1.annotate('',
    #              (IPOC_B1.index[26000], 1.5),
    #              xycoords='data',
    #              xytext=(-40, -40),
    #              textcoords='offset points',
    #              arrowprops=dict(ec="k", fc='k'))
    # fig.autofmt_xdate()
    # plt.tight_layout()
    plt.show()


def create_PCA_plot():
    client = mongo_client.CERNMongoClient()
    start_date = '2016-04-16 00:00:00.000000'
    end_date = '2016-09-14 00:00:00.000000'
    query_B1 = "^MKI.*\.B1:.*"
    query_B2 = "^MKI.*\.B2:.*"
    data_B1 = client.query_resample(query_B1, start_date, end_date, '30T')
    data_B2 = client.query_resample(query_B2, start_date, end_date, '30T')

    data_B1 = preprocessing.filter_extremes(data_B1)
    data_B2 = preprocessing.filter_extremes(data_B2)

    data_B1_s = preprocessing.scale_data(data_B1)
    data_B2_s = preprocessing.scale_data(data_B2)

    pca= PCA.create_pca(2)
    pca_data_B1 = PCA.transform_data(data_B1, pca)
    pca_data_B2 = PCA.transform_data(data_B2, pca)
    pca_data_B1_s = PCA.transform_data(data_B1_s, pca)
    pca_data_B2_s = PCA.transform_data(data_B2_s, pca)
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(nrows=2, ncols=2, figsize=(15, 8))
    ax1.scatter(pca_data_B1[:, 0], pca_data_B1[:, 1], color='navy', alpha=.8)
    ax2.scatter(pca_data_B2[:, 0], pca_data_B2[:, 1], color='navy', alpha=.8)
    ax3.scatter(pca_data_B1_s[:, 0], pca_data_B1_s[:, 1], color='navy', alpha=.8)
    ax4.scatter(pca_data_B2_s[:, 0], pca_data_B2_s[:, 1], color='navy', alpha=.8)

    ax1.set_title("PCA data MKI2")
    ax2.set_title("PCA data MKI8")
    ax3.set_title("PCA geschaalde data MKI2")
    ax4.set_title("PCA geschaalde data MKI8")

    plt.tight_layout()
    plt.show()



plot_cont_B1_B2("TEMP_MAGNET_UP", u"°C", True)
# plot_IPOC_B1_B2('I_STRENGTH', u'kA', True)
# create_PCA_plot()