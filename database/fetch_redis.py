import ast
import sys

import pandas as pd
import redis


def connect():
    """
    Connects to Redis (local)
    :return: conn or exits when no connection can be made
    """
    conn = redis.Redis('localhost')
    if not conn.ping():
        print("no redis server found")
        sys.exit()
    else:
        print("connected to redis server")
        return conn


def get_measurement_types(conn):
    """
    Get all the different measurement types in the database
    :param conn: conn to database
    :return: List with strings of all measurement types
    """
    return conn.hkeys("all_3m")


def get_measurements(conn, pattern):
    """
    Queries the database and returns data belonging to the fields specified by pattern
    :param conn: conn to database
    :param pattern: string, can be specific (MKI.A5R8.B2:PRESSURE) or with wildcards (*PRESSURE)
    :return: data in dataframe, columns are all measurements with 2 rows, 1 with the timestamps and 1 with the values
    """
    print("Fetching data for pattern %s" % pattern)
    key = "all_3m"
    df = pd.DataFrame()
    for measurement in conn.hscan_iter(key, match=pattern):
        var = measurement[0]
        print("Fetching data for %s" % var)
        data = measurement[1]
        data = data.decode("utf-8")
        data = ast.literal_eval(data)
        timestamps = data[0]
        values = data[1]
        df[var] = pd.Series([timestamps, values], index=['timestamps', 'values'])
    return df


def compress_data(data, sample_size='30Min'):
    """
    Compresses the data by sampling and averaging
    :param data: data as given by get_measurements
    :param sample_size: string eg '30Min': all timestamps will average over sample_size
    :return: Dataframe with first column the new timestamps and other columns the measurements, rows containing values
    """
    df = pd.DataFrame()
    for measurement in data:
        index = pd.to_datetime(pd.Series(data.loc["timestamps", measurement]), unit="s")
        series = pd.Series(data.loc["values", measurement], index=index)
        series = series.resample(sample_size).mean()
        df[measurement] = series
    df.reset_index(inplace=True)
    return df


def filter_IPOC(data, threshold):
    """
    Filters incorrect measurements
    :param data: data as given by get_measurements (should be IPOC data!)
    :param threshold: maximum absolute value allowed
    :return: data with incorrect measurements filtered out
    """
    for measurement in data:
        timestamps = data.loc["timestamps", measurement]
        values = pd.Series(data.loc["values", measurement], index=timestamps)
        values = values[values.abs() < threshold]
        data.loc["timestamps", measurement] = values.index.values
        data.loc["values", measurement] = list(values)

    return data
