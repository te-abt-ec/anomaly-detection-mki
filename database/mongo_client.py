import re
import warnings

import pandas as pd
import pymongo as pm


class CERNMongoClient(object):
    """
    Client to connect with the MongoDB.
    Provides functions to get data, compress it and filter extremes
    """

    def __init__(self, host='localhost', port=27017, db='cern_ldb', logging=True):
        """
        :param host: default localhost
        :param port: default 27017
        :param db: database name, default cern_ldb
        """
        self.client = pm.MongoClient(host, port)
        self.db = self.client[db]
        self.logging = logging

    def get_all_collections(self):
        """
        :return: list of strings with all collection names
        """
        return self.db.collection_names()

    def create_starttime_index(self, collection_name):
        """
        Temporary function to add an index on the "start-time" field on a collection
        Changed by PVT to refer to existing 'start-time' key/field
        Checking for existing index not needed (idempotent opeation)
        :param collection_name: string defining the collection
        :return: void
        """
        inn = "start-time"
        collection = self.db[collection_name]
        collection.create_index(inn)
        #collection.create_index([('start_time', pm.ASCENDING)], name='start_time', default_language='english')
        if self.logging:
            for i in collection.list_indexes():
                print(i)        

    def get_measurements_pattern(self, pattern):
        """
        Get all the measurements from 1 pattern in the database.
        Example: pattern = "^.*TEMP_TUBE_UP$"
        :param pattern: regex string, can be specific ("MKI.A5R8.B2:PRESSURE") or with wildcards ("^.*PRESSURE$")
        :return: data in dataframe, columns are all measurements with 2 rows,
        1 with the timestamps and 1 with the values
        """
        warnings.warn("use query_resample instead", DeprecationWarning)
        collections = [m.group(0) for l in self.get_all_collections() for m in [re.search(pattern, l)] if m]
        if self.logging: print("Fetching data for pattern %s" % pattern)

        df = pd.DataFrame()
        for collection in collections:
            if self.logging: print("Fetching data for %s" % collection)
            data = self.get_all_measurements_collection(collection)
            df[collection] = pd.Series([data['timestamps'], data['values']], index=['timestamps', 'values'])
        return df

    def get_all_measurements_collection(self, collection_name):
        """
        Gets all the timestamps and values in order from 1 collection
        :param collection_name: the collection to query
        :return: dictionary with keys 'timestamps' and 'values', fields are lists
        """
        warnings.warn("use query_resample_collection instead", DeprecationWarning)
        collection = self.db[collection_name]
        timestamps = []
        values = []
        for doc in collection.find().sort('start-time'):
            timestamps.extend(doc['timestamps'])
            values.extend(doc['values'])
        return {'timestamps': timestamps, 'values': values}

    def query_resample(self, pattern,  start_time, end_time, sample_size=None):
        """
        Queries all collections matching the pattern and resamples the data in a single dataframe
        Missing values will be padded with the previous value or the next in case a series starts with NaN
        :param pattern: regex string, can be specific ("MKI.A5R8.B2:PRESSURE") or with wildcards ("^.*PRESSURE$")
        :param start_time: first entry according to start_time field
        :param end_time: last entry (not included) according to start_time field
        :param sample_size: String, number + 'D' for days, 'T' for minutes, 'S' for seconds
        :return: resampled dataframe, columns are collections
        """
        collections = [m.group(0) for l in self.get_all_collections() for m in [re.search(pattern, l)] if m]
        if self.logging: print("Fetching data for pattern {0} from {1} to {2}".format(pattern, start_time, end_time))

        df = pd.DataFrame()
        for collection in collections:
            if self.logging: print("Fetching data for %s" % collection)
            data = self.query_resample_collection(collection, start_time, end_time, sample_size)
            df = pd.concat([df, data], axis=1)

        # Hold values in case of NaN, first forwards then backwards in case first value is NaN
        df = df.fillna(method='pad')
        df = df.fillna(method='backfill')
        return df

    def query_resample_collection(self, collection_name, start_time, end_time, sample_size):
        """
        Queries a collections  and resamples the data in a  dataframe
        :param collection_name: name of the collection
        :param start_time: first entry according to start_time field
        :param end_time: last entry (not included) according to start_time field
        :param sample_size: String, number + 'D' for days, 'T' for minutes, 'S' for seconds
        :return: resampled dataframe, columns is collection
        """
        collection = self.db[collection_name]
        if collection.count() == 0:
            print("Error: Empty or non-existing collection!")
        timestamps = []
        values = []
        docs = collection.find({
            'start-time': {
                '$gte': start_time,
                '$lt': end_time
            }
        }).sort('start-time')

        for doc in docs:
            timestamps.extend(doc['timestamps'])
            values.extend(doc['values'])
        df = pd.DataFrame({collection_name: values}, index=pd.to_datetime(timestamps, unit='s'))\
            .rename_axis('timestamps').rename_axis("series", axis="columns")
        if sample_size is not None:
            df = df.resample(sample_size).mean()
        return df

    def query_tagged_anomalies(self, start_time, end_time):
        """
        Creates a nicely formated dataframe with all tagged anomalies in the E-logbook
        :param start_time: start date of tags
        :param end_time: end date of tags
        :return: df with timestamps, tagged time, id, path, comment, tag username and value
        """
        # start-time index does not seem to work, even when creating it here
        collection = "MKI.ELOGBOOK_tagged"
        ELB = self.query_resample(collection, start_time, end_time)
        ELB = ELB.join(ELB[collection].apply(pd.Series))
        ELB.drop(collection, axis=1, inplace=True)
        anom = ELB[ELB["TAG"] == "anomaly"]
        return anom

    def query_elogbook(self, start_time, end_time):
        """
        Creates a nicely formated dataframe of the E-logbook
        :param start_time: start date of tags
        :param end_time: end date of tags
        :return: df with timestamps, tagged time, id, path, comment, tag username and value
        """
        collection = "MKI.ELOGBOOK_tagged"
        ELB = self.query_resample(collection, start_time, end_time)
        ELB = ELB[collection].apply(pd.Series)
        return ELB

    @staticmethod
    def compress_measurements(data, sample_size='30Min'):
        """
        Compresses the data by sampling and averaging
        :param data: data as given by get_measurements
        :param sample_size: string eg '30Min': all timestamps will average over sample_size
        :return: Dataframe with first column the new timestamps and other columns the measurements,
        rows containing values
        """
        df = pd.DataFrame()
        for measurement in data:
            index = pd.to_datetime(pd.Series(data.loc["timestamps", measurement]), unit="s")
            series = pd.Series(data.loc["values", measurement], index=index)
            series = series.resample(sample_size).mean()
            df[measurement] = series
        df.reset_index(inplace=True)
        return df

    @staticmethod
    def filter_extremes(data, threshold):
        """
        Filters incorrect measurements, use before compressing!
        :param data: data as given by get_measurements_pattern or get_all_measurements_collection
        :param threshold: maximum absolute value allowed
        :return: data with incorrect measurements filtered out
        """
        for measurement in data:
            timestamps = data.loc["timestamps", measurement]
            values = pd.Series(data.loc["values", measurement], index=timestamps)
            values = values[values.abs() < threshold]
            data.loc["timestamps", measurement] = values.index.values
            data.loc["values", measurement] = list(values)

        return data
