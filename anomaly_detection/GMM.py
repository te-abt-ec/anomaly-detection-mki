import itertools

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import LogNorm
from scipy import linalg
from sklearn import mixture


def component_selection(data, title='BIC score per model', max_range=8, cv_types=['spherical', 'tied', 'diag', 'full']):
    """
    Creates a plot showing the BIC scores for different models (1-max_range components, all covariance matrices)
    :param cv_types: different types of covariance matrix
    :param data: data resampled to 1 frequency
    :param title: title for the plot
    :param max_range: maximum for component range
    :return: nothing, prints the plot
    """
    lowest_bic = np.infty
    bic = []
    n_components_range = range(1, max_range + 1)
    # covariance matrix types
    # documentation:
    # 'full' (each component has its own general covariance matrix),
    # 'tied' (all components share the same general covariance matrix),
    # 'diag' (each component has its own diagonal covariance matrix),
    # 'spherical' (each component has its own single variance).
    for cv_type in cv_types:
        for n_components in n_components_range:
            # Fit a Gaussian mixture with EM
            gmm = mixture.GaussianMixture(n_components=n_components,
                                          covariance_type=cv_type)
            gmm.fit(data)
            bic.append(gmm.bic(data))

    bic = np.array(bic)
    color_iter = itertools.cycle(['navy', 'turquoise', 'cornflowerblue',
                                  'darkorange'])
    bars = []

    # Plot the BIC scores
    spl = plt.subplot(1, 1, 1)
    for i, (cv_type, color) in enumerate(zip(cv_types, color_iter)):
        xpos = np.array(n_components_range) + .2 * (i - 2)
        bars.append(plt.bar(xpos, bic[i * len(n_components_range):
                                      (i + 1) * len(n_components_range)],
                            width=.2, color=color))
    plt.xticks(n_components_range)
    plt.ylim([bic.min() * 1.01 - .01 * bic.max(), bic.max()])
    plt.title(title)
    xpos = np.mod(bic.argmin(), len(n_components_range)) + .65 +\
        .2 * np.floor(bic.argmin() / len(n_components_range))
    plt.text(xpos, bic.min() * 0.97 + .03 * bic.max(), '*', fontsize=14)
    spl.set_xlabel('Number of components')
    spl.legend([b[0] for b in bars], cv_types)
    plt.show()


def log_likelihood_selection(data, title='Log likelihood per model', max_range=8,
                             cv_types=['spherical', 'tied', 'diag', 'full']):
    likelihoods = []
    n_components_range = range(1, max_range + 1)
    # covariance matrix types
    # documentation:
    # 'full' (each component has its own general covariance matrix),
    # 'tied' (all components share the same general covariance matrix),
    # 'diag' (each component has its own diagonal covariance matrix),
    # 'spherical' (each component has its own single variance).
    for cv_type in cv_types:
        for n_components in n_components_range:
            # Fit a Gaussian mixture with EM
            gmm = mixture.GaussianMixture(n_components=n_components,
                                          covariance_type=cv_type)
            gmm.fit(data)
            likelihoods.append(gmm.score(data))

    likelihoods = np.array(likelihoods)
    color_iter = itertools.cycle(['navy', 'turquoise', 'cornflowerblue',
                                  'darkorange'])
    bars = []

    spl = plt.subplot(1, 1, 1)
    for i, (cv_type, color) in enumerate(zip(cv_types, color_iter)):
        xpos = np.array(n_components_range) + .2 * (i - 2)
        bars.append(plt.bar(xpos, likelihoods[i * len(n_components_range):
        (i + 1) * len(n_components_range)],
                            width=.2, color=color))
    plt.xticks(n_components_range)
    plt.ylim([likelihoods.min() * 1.01 - .01 * likelihoods.max(), likelihoods.max()])
    plt.title(title)
    spl.set_xlabel('Number of components')
    spl.legend([b[0] for b in bars], cv_types)


def get_top_N_anomalies(log_probs, N):
    """
    Get the indexes of the top N anomalies
    :param log_probs: output of GMM.score_samples
    :param N: number of anomalies
    :return: idx of top N anomalies
    """
    top_N = np.argsort(log_probs)[:N]
    return top_N


def add_sorted_anomaly_probs(clf, feature_df):
    """
    Adds the log probabilities as an extra column ("log_prob") and sorts ascending.
    :param clf: GMM
    :param feature_df: df with same dimensions as the one GMM is trained on
    :return: feature_df with log_prob sorted ascending
    """
    log_probs = clf.score_samples(feature_df)
    feature_df["log_prob"] = log_probs
    return feature_df.sort_values(by="log_prob")







