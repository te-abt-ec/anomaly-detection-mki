#!/usr/bin/python3.4
"""
Created by Pieter Van Trappen, CERN
Convert xml data from CERN's eLogbook to mongodb data
for anomaly detection

./xml_to_mongodb.py 3months_export.xml MKI.ELOGBOOK 3m
"""

#TODO: automate the '&quot;' removal for the CLOB (done with emacs now)
# or rather rename the column in sql - doesn't seem straightforward?

import xml.etree.ElementTree as ET
import sys
from fetch_data3 import mongoDB
from datetime import datetime

if not len(sys.argv)==4:
    print("please specify: %s xml-file collection-name length" % sys.argv[0])
    sys.exit()

tree = ET.parse(sys.argv[1])
root = tree.getroot() #rowdata

data = []
timestamps = []
for row in root:
    #print(list(row))
    dic = {}
    for k in row.iter():
        # strange but 'ROW' is in there as well
        if (not (k.tag == 'ROW' or k.tag=='DBMS_LOB')):
            dic[k.tag] = k.text
            # format depends on PC from which xml extraction was done!
            # %I:%M:%S %p in case of am/pm
            # 23/04/2016 06:00:00
            if k.tag=='EVENTDATE':
                # convert sql date (string) to epoch
                timestamps.append(datetime.strptime(k.text,'%d/%m/%Y %H:%M:%S').timestamp())
    data.append(dic)

print("%d entries loaded from xml file" % len(data))
#for i,e in enumerate(data):
#    print(timestamps[i])
#    print(e.keys())

conn = mongoDB()
var = sys.argv[2]
startt = datetime.fromtimestamp(timestamps[0]).strftime('%Y-%m-%d %H:%M:%S.%f')
if not conn.exist_doc(var,startt,sys.argv[3]):
    conn.write_doc(var,[timestamps,data],startt,sys.argv[3])
    print('Written to mongodb')
else:
    print('Existing document, nothing written out')

#print(conn.db[var].find_one())
    
