# If a plot or something does not directly belong in another file, put it here
# Eg plots in pca dimensions go in PCA file
# Plot concerning timeseries go here
import matplotlib.pyplot as plt


def plot_anomalies_timeseries(df, anomaly_idx, series_name, title='Anomalies plotted on timeseries', lstyle='solid', mrk=','):
    """
    :param lstyle: linestyle for series plotting, see matplotlib e.g. solid, dashed, none
    :parm mrk: marker for series plotting, see matplotlib e.g. ',' for pixel, 'o' for circle
    http://matplotlib.org/api/lines_api.html
    """
    fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(12, 8))
    regex = '^MKI.*:%s$' % series_name
    timeseries = df.filter(regex=regex)
    timeseries.plot(ax=axes, colormap='winter', kind='line', marker=mrk, linestyle=lstyle)
    anomalies_timeseries = df.iloc[anomaly_idx.tolist()]
    for series in timeseries:
        plt.scatter(anomalies_timeseries.index, anomalies_timeseries.loc[:, series], marker='o', \
                    color='red', zorder=99)
    plt.title(title)
