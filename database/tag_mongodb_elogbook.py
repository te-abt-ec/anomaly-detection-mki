#!/usr/bin/python3.4
"""
Created by Pieter Van Trappen, CERN

Fetches the single elogbook document from the mongodb collection and
asks the user for tagging data: info, intervention, research (MD, ..), etc.

./tag_mongodb_elogbook.py MKI.ELOGBOOK

#TODO: Add 'anomaly' to indicate what's detectable with the current data? 
"""

import sys
from fetch_data3 import mongoDB
from datetime import datetime
uoptions = {'i':'info', 'v':'intervention', 'r':'research','f':'fault', 'a':'anomaly', 'q':'*quit*'}

if not len(sys.argv)==2:
    print("please specify: %s collection-name" % sys.argv[0])
    sys.exit()

conn = mongoDB()
var = sys.argv[1]
vart = var+'_tagged'
if vart in conn.db.collection_names() and conn.db[vart].count()>0:
    print('-----------------------------------------------------------------------------')
    print('Existing %s collection found, will be used' % vart)
    coll = conn.db[vart]
elif conn.db[var].count()>0:
    coll = conn.db[var]
else:
    print('No collections with a valid document found, quiting')
    sys.exit()
    
if coll.count()>1:
    print('More than 1 document found, aborting...')
    sys.exit()
doc = coll.find_one()
# loop over the list of dictionaries in 'values'
for e in doc['values']:
    print('-----------------------------------------------------------------------------')
    print(e['EVENTDATE'])
    print(e['USERNAME'])
    print(e['SUBSTR_COMMENT_512_'])
    print('-----------------------------------------------------------------------------')
    print(uoptions)
    # recover previous tag if in uoptions dict
    if e['TAG'] in uoptions.values():
        tag = [k for k,v in uoptions.items() if v==e['TAG']][0]
    else:
        tag = 'i'
    print("Please specify logbook entry tag (%s): " % tag)
    uchoice = input()
    if not (uchoice=='' or uchoice in uoptions):
        uchoice = input("Please specify logbook entry tag (i): ")
    # default 'info' when enter pressed
    uchoice = tag if uchoice=='' else uchoice
    if uchoice=='q':
        break
    # replace existing value for TAG
    e['TAG'] = uoptions[uchoice]
        

# write out to new collection (.._tagged)
startt = doc['start-time']
samplesize = doc['length']
if conn.exist_doc(vart,startt,samplesize):
    uchoice = input('Existing document, do you want to overwrite (y,n): ')
    if uchoice=='n':
        sys.exit()
conn.db[vart].delete_many({'start-time':startt})        
conn.write_doc(vart,[doc['timestamps'],doc['values']],startt,samplesize)
print('Written to mongodb')

