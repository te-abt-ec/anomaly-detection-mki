import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def plot_B1_B2(data, meas_type, unit):
    """
    Creates a plot comparing data of beam 1 with data of beam 2
    :param data: first column timestamps, next columns data of measurments
    :param meas_type: title of plot
    :param unit: unit of yscale
    :return: pyplot plot
    """
    timestamps = data.loc[:, "index"]
    B1 = data.filter(regex="^.*.B1.*$")
    B2 = data.filter(regex="^.*.B2.*$")
    plt.figure(1)
    plt.subplot(121)
    for curve in B1:
        label = str(curve).split(':')[0]
        plt.plot(timestamps, B1.loc[:, curve], label=label)
    plt.xticks(rotation="vertical")
    plt.subplots_adjust(bottom=0.45)
    plt.title("%s: B1" % meas_type)
    plt.ylabel("%s in %s" % (meas_type, unit))
    plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.45))
    plt.subplot(122)
    for curve in B2:
        label = str(curve).split(':')[0]
        plt.plot(timestamps, B2.loc[:, curve], label=label)
    plt.xticks(rotation="vertical")
    plt.subplots_adjust(bottom=0.45)
    plt.title("%s: B2" % meas_type)
    plt.ylabel("%s in %s" % (meas_type, unit))
    plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.45))
    return plt


def plot_IPOC(data, meas_type, unit):
    """
    Creates a scatterplot of IPOC data
    :param data: formal: columns with data, 2 rows: timestamps and values
    :param meas_type: title
    :param unit: unit of yscale
    :return: pyplot plot
    """
    fig = plt.figure(1)
    ax = fig.add_subplot(111)
    for meas in data:
        label = str(meas).split(':')[0]
        ax.scatter(pd.to_datetime(data.loc["timestamps", meas], unit='s'), data.loc["values", meas], label=label)
    plt.xticks(rotation="vertical")
    plt.subplots_adjust(bottom=0.45)
    plt.title(meas_type)
    plt.ylabel("%s in %s" % (meas_type, unit))
    colormap = plt.cm.gist_ncar
    colorst = [colormap(i) for i in np.linspace(0, 0.9, len(ax.collections))]
    for t, j1 in enumerate(ax.collections):
        j1.set_color(colorst[t])
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.35), fontsize='small')
    return ax


def extract_between_thresholds(data, min, max, resample='1Min'):
    """
    Extracts data between given thresholds
    :param data: data as given by get_measurement_pattern
    :param min: min threshold
    :param max: max threshold
    :param resample: resample data and aggregate mean
    :return: dataframe same format as get_measurement_pattern
    """

    for meas in data:
        df = pd.DataFrame({meas: data.loc["values", meas]},
                          index=pd.to_datetime(pd.Series(data.loc["timestamps", meas]), unit="s"))

        df = df[(df[meas] >= min) & (df[meas] <= max)]
        df = df.resample(resample).mean()
        df = df.dropna()
        data.loc["values", meas] = df[meas].tolist()
        data.loc["timestamps", meas] = df.index.tolist()
    return data
