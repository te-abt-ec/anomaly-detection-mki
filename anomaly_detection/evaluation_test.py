import unittest

import pandas as pd

import evaluation


class AnomalySegmenterTester(unittest.TestCase):
    def setUp(self):
        self.segmenter = evaluation.AnomalySegmenter()
        self.test_data = pd.DataFrame(data=None,
                                      index=['2016-06-01 11:00:00',
                                             '2016-06-01 12:00:00',
                                             '2016-06-03 12:00:00',
                                             '2016-06-10 13:00:00',
                                             '2016-06-10 08:00:00',
                                             '2016-06-01 23:00:00',
                                             '2016-06-02 02:00:00'])
        self.test_data.index = pd.to_datetime(self.test_data.index, yearfirst=True)

    def test_create_segments(self):
        s1 = ['2016-06-01 11:00.00', '2016-06-01 12:00.00', '2016-06-01 23:00.00', '2016-06-02 02:00.00']
        s2 = ['2016-06-03 12:00.00']
        s3 = ['2016-06-10 08:00.00', '2016-06-10 13:00.00']
        segments = [segment.format_ts() for segment in self.segmenter.create_segments(self.test_data)]
        self.assertEqual(3, len(segments))
        self.assertIn([unicode(i) for i in s1], segments)
        self.assertIn([unicode(i) for i in s2], segments)
        self.assertIn([unicode(i) for i in s3], segments)

    def test_expand_segments(self):
        new_timestamps = pd.DataFrame(data=None,
                                      index=pd.to_datetime(['2016-06-01 13:00:00',
                                                            '2016-06-10 14:00:00',
                                                            '2016-06-10 09:00:00',
                                                            '2016-06-03 14:00:00']))
        self.segmenter.create_segments(self.test_data)
        unsegmented = self.segmenter.expand_segments(new_timestamps)
        s1 = ['2016-06-01 11:00.00', '2016-06-01 12:00.00', '2016-06-01 13:00.00', '2016-06-01 23:00.00',
              '2016-06-02 02:00.00']
        s2 = ['2016-06-03 12:00.00']
        s3 = ['2016-06-10 08:00.00', '2016-06-10 09:00.00', '2016-06-10 13:00.00']
        segments = [segment.format_ts() for segment in self.segmenter.segments]
        self.assertEqual(3, len(segments))
        self.assertIn([unicode(i) for i in s1], segments)
        self.assertIn([unicode(i) for i in s2], segments)
        self.assertIn([unicode(i) for i in s3], segments)
        self.assertTrue(pd.to_datetime(['2016-06-10 14:00:00', '2016-06-03 14:00:00']).equals(unsegmented.index))

    def test_discard_timestamps(self):
        self.segmenter.create_segments(self.test_data)
        new_timestamps = pd.DataFrame(data=None,
                                      index=pd.to_datetime(['2016-06-01 13:00:00',
                                                            '2016-06-10 14:00:00',
                                                            '2016-06-11 01:01:00',
                                                            '2016-06-03 14:00:00',
                                                            '2016-06-04 02:00:00',
                                                            '2016-06-10 07:00:00',
                                                            '2016-06-09 12:00:00']))
        remaining = self.segmenter.discard_timestamps(new_timestamps)
        expected = pd.to_datetime(
            ['2016-06-01 13:00:00', '2016-06-11 01:01:00', '2016-06-04 02:00:00', '2016-06-09 12:00:00'])
        self.assertTrue(expected.equals(remaining.index))

    def test_segments_generator(self):
        data = pd.DataFrame(data=None,
                            index=pd.to_datetime(['2016-06-01 11:00:00',
                                                  '2016-06-01 12:00:00',
                                                  '2016-06-03 12:00:00',
                                                  '2016-06-10 13:00:00',
                                                  '2016-06-10 08:00:00',

                                                  '2016-06-01 23:00:00',
                                                  '2016-06-02 02:00:00',
                                                  '2016-06-01 11:30:00',
                                                  '2016-06-02 07:00:00',
                                                  '2016-06-15 15:00:00']))
        generator = self.segmenter.segments_generator(data, [5, 10])
        segments = generator.next()
        s1 = ['2016-06-01 11:00.00', '2016-06-01 12:00.00']
        s2 = ['2016-06-03 12:00.00']
        s3 = ['2016-06-10 08:00.00', '2016-06-10 13:00.00']
        segments = [segment.format_ts() for segment in segments]
        self.assertEqual(3, len(segments))
        self.assertIn([unicode(i) for i in s1], segments)
        self.assertIn([unicode(i) for i in s2], segments)
        self.assertIn([unicode(i) for i in s3], segments)

        segments = generator.next()
        s1 = ['2016-06-01 11:00.00', '2016-06-01 11:30.00', '2016-06-01 12:00.00']
        s2 = ['2016-06-03 12:00.00']
        s3 = ['2016-06-10 08:00.00', '2016-06-10 13:00.00']
        s4 = ['2016-06-02 02:00.00', '2016-06-02 07:00.00']
        s5 = ['2016-06-15 15:00.00']
        segments = [segment.format_ts() for segment in segments]
        self.assertEqual(5, len(segments))
        self.assertIn([unicode(i) for i in s1], segments)
        self.assertIn([unicode(i) for i in s2], segments)
        self.assertIn([unicode(i) for i in s3], segments)
        self.assertIn([unicode(i) for i in s4], segments)
        self.assertIn([unicode(i) for i in s5], segments)

    def test_segments_generator2(self):
        # tests 2nd way to use generator
        data = pd.DataFrame(data=None,
                            index=pd.to_datetime(['2016-06-01 11:00:00',
                                                  '2016-06-01 12:00:00',
                                                  '2016-06-03 12:00:00',
                                                  '2016-06-10 13:00:00',
                                                  '2016-06-10 08:00:00',

                                                  '2016-06-01 23:00:00',
                                                  '2016-06-02 02:00:00',
                                                  '2016-06-01 11:30:00',
                                                  '2016-06-02 07:00:00',
                                                  '2016-06-15 15:00:00']))
        segments = list(self.segmenter.segments_generator(data, [5, 10]))
        s1 = ['2016-06-01 11:00.00', '2016-06-01 12:00.00']
        s2 = ['2016-06-03 12:00.00']
        s3 = ['2016-06-10 08:00.00', '2016-06-10 13:00.00']
        segments0 = [segment.format_ts() for segment in segments[0]]
        self.assertEqual(3, len(segments0))
        self.assertIn([unicode(i) for i in s1], segments0)
        self.assertIn([unicode(i) for i in s2], segments0)
        self.assertIn([unicode(i) for i in s3], segments0)

        s1 = ['2016-06-01 11:00.00', '2016-06-01 11:30.00', '2016-06-01 12:00.00']
        s2 = ['2016-06-03 12:00.00']
        s3 = ['2016-06-10 08:00.00', '2016-06-10 13:00.00']
        s4 = ['2016-06-02 02:00.00', '2016-06-02 07:00.00']
        s5 = ['2016-06-15 15:00.00']
        segments1 = [segment.format_ts() for segment in segments[1]]
        self.assertEqual(5, len(segments1))
        self.assertIn([unicode(i) for i in s1], segments1)
        self.assertIn([unicode(i) for i in s2], segments1)
        self.assertIn([unicode(i) for i in s3], segments1)
        self.assertIn([unicode(i) for i in s4], segments1)
        self.assertIn([unicode(i) for i in s5], segments1)


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(AnomalySegmenterTester)
    runner = unittest.TextTestRunner()
    runner.run(suite)
