# -*- coding: utf-8 -*-
import itertools
import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn import mixture

from anomaly_detection import preprocessing

plt.style.use('ggplot')


def create_component_plot():
    filename = "all_features_(B1)_2017-04-09"  # 30min, B1
    filename = os.path.join('..', 'CSV', filename + ".csv")
    B1_30 = pd.read_csv(filename, index_col=0)
    B1_30.index = pd.to_datetime(B1_30.index)
    data = preprocessing.scale_robust(B1_30)

    lowest_bic = np.infty
    bic = []
    n_components_range = range(1, 5 + 1)
    cv_types = ['spherical', 'tied', 'diag']
    # covariance matrix types
    # documentation:
    # 'full' (each component has its own general covariance matrix),
    # 'tied' (all components share the same general covariance matrix),
    # 'diag' (each component has its own diagonal covariance matrix),
    # 'spherical' (each component has its own single variance).
    for cv_type in cv_types:
        for n_components in n_components_range:
            # Fit a Gaussian mixture with EM
            gmm = mixture.GaussianMixture(n_components=n_components,
                                          covariance_type=cv_type)
            gmm.fit(data)
            bic.append(gmm.bic(data))

    bic = np.array(bic)
    color_iter = itertools.cycle(['navy', 'turquoise', 'cornflowerblue',
                                  'darkorange'])
    bars = []

    # Plot the BIC scores
    spl = plt.subplot(1, 1, 1)
    for i, (cv_type, color) in enumerate(zip(cv_types, color_iter)):
        xpos = np.array(n_components_range) + .2 * (i - 2)
        bars.append(plt.bar(xpos, bic[i * len(n_components_range):
                                      (i + 1) * len(n_components_range)],
                            width=.2, color=color))
    plt.xticks(n_components_range)
    plt.ylim([bic.min() * 1.01 - .01 * bic.max(), bic.max()])
    # xpos = np.mod(bic.argmin(), len(n_components_range)) + .65 +\
    #     .2 * np.floor(bic.argmin() / len(n_components_range))
    # plt.text(xpos, bic.min() * 0.97 + .03 * bic.max(), '*', fontsize=14)
    spl.set_xlabel('Aantal componenten')
    spl.set_ylabel('BIC score')
    spl.legend([b[0] for b in bars], cv_types)
    plt.show()


create_component_plot()